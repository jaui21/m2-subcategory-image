# m2-subcategory-image

This custom module will allow the user to show subcategory images in the category view

ADD THIS IN [YOURTHEME]/[THEME]/Magento_Catalog/layout/catalog_category_view.xml
** inside <referenceContainer name="content">

 <block class="Jaui\SubcategoryImage\Block\CategoryCollect" name="subcategory.image" after="-" template="Jaui_SubcategoryImage::categorycollect.phtml" />